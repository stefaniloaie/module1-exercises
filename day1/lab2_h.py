def un_flatten(paths):
    d = {}
    return d


def test_un_flatten():
    tree = un_flatten(['A/B/T', 'A/U', 'A/U/Z'])
    assert tree == {
        'A': {
            'B': {'T': {}},
            'U': {
                'Z': {}
            }
        }
    }
    assert tree['A']['B']['T'] == {}
